# Concentric Tube Robot - Kinematic Library

This is a concentric tube robot (CTR) kinematics library. The library implements highly efficient code for the forward kinematics of concentric tube robots.

### Prerequisites

- Eigen 3.3. (header-only), 
- Erl (header-only, https://gitlab.com/ggras/Erl), 
- boost (header-only)

C++14 support (gcc-6, clang-4).

### Installing

Install with cmake. A script to download prerequists, compile and install is avaiable with instal.sh

## License

This project is licensed under the GPL v3 License - see the [LICENSE](LICENSE) file for details.

